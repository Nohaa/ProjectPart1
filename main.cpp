#include <iostream>
#include <fstream>
using namespace std;
class Task
{
  private:
   string name ,description, due_date;
   int priority ,numOfDays;
   Task *next;     // to hold pointer to next item

   public:
      Task ()
      { next=0;}

   void setnext(Task* aNext)
   {
        next = aNext;
   }
  ////////////////////////////////////////////////////
  Task *getnext()
  {
      return next;
  }
  ////////////////////////////////////////////////////
  void setname(string taskName)
   {
     name=taskName;
   }
   string getname()
   {
       return name;
   }
   ///////////////////////////////////////////////////
   void setdescription(string desc)
   {
     description=desc;
   }
   string getdescription()
   {
       return description;
   }
   ///////////////////////////////////////////////////
   void setpriority(int priortty)
   {
     priority=priortty;
   }
   int getpriority()
   {
       return priority;
   }
   ///////////////////////////////////////////////////
   void setdue_date(string date)
   {
     due_date=date;
   }
   string getdue_date()
   {
       return due_date;
   }
   ///////////////////////////////////////////////////
   void setnumOfDays(int days)
   {
     numOfDays=days;
   }
   int getnumOfDays()
   {
       return numOfDays;
   }
   ////////////////////////////////////////////////////
  friend ostream& operator<<(ostream& obj,Task& tsk);
};
///////////////////////////////////////////////////

////////////////////////////////////////////////
ostream& operator << (ostream & obj, Task & tsk)
{
obj<<tsk.getname()<<endl;
obj<<tsk.getdescription()<<endl;
obj<<tsk.getpriority()<<endl;
obj<<tsk.getdue_date()<<endl;
//obj<<tsk.getdue_date()<endl;
obj<<tsk.getnumOfDays()<<endl;
return obj;
}


template <class T>
class Node {
  protected:
	T     data;        // to hold generic data of type T
	Node<T>* next;     // to hold pointer to next item
	Node<T>* prev;     // to hold pointer for previous item
	int   count;
  public:
		Node () {next = prev = NULL; count = 0;}
		Node (T dataItem, Node<T>* nextPtr = NULL, Node<T>* prevPtr = NULL) {
            data = dataItem; next = nextPtr; prev = prevPtr;
        }
		Node<T>* getNext ()        {return next;}
		void setNext(Node<T>* ptr) {next = ptr;}
		Node<T>* getPrev ()        {return prev;}
		void setPrev(Node<T>* ptr) {prev = ptr;}
		T  getData ()              {return data;}
		void setData (T dataItem)  {data = dataItem;}
		template <class TT>
		friend ostream& operator<< (ostream& out, Node<TT> n);
};
/////////////////////////////////////////////////////////////////
template <class T>
ostream& operator<< (ostream& out, Node<T> node) {
    out << node.getData() << "->";
    return out;
}

////////////////////////////////
template <class T>
class FCISLL {
    private:
        Node<T> *head, *tail;
        Task *headTask,*priviosTask;
    public:
        FCISLL() {head = NULL; tail = NULL;}
        ~FCISLL ();
        void addToHead(T item);
        void addToTail(T item);
        void addToIndex(T item, int index);
        void createLinkedList(Task *data);//dy my proj w hn3ml beha 3shan nktb 3la el file
        void save(Task *ptr);
        Task readTasks();
        bool search (T item);
        template <class TT>
        friend ostream& operator<<(ostream&, FCISLL<TT>);
};

////////////////////////////////////////////////////////////////////////////////

template <class T>
void FCISLL<T>::addToTail(T item) {
    Node<T>* ptr = new Node<T>(item, NULL);
    if (head == NULL)
        head = tail = ptr;
    else {
        tail->setNext(ptr);
        tail = ptr;
    }
}

template <class T>
void FCISLL<T>::addToHead(T item) {
    Node<T>* ptr = new Node<T>(item, head);
    head = ptr;
    if (tail == NULL)
        tail = ptr;
}

template <class T>
FCISLL<T>::~FCISLL () {}

template <class T>
void FCISLL<T>::addToIndex(T item, int index) {
    Node<T>* nPtr = new Node<T>(item, NULL);
    Node<T>* pPtr = head;
    for (int i = 0; pPtr != 0 && i < index - 1; i++)
       pPtr = pPtr->getNext();
    if (pPtr != 0 && !(index < 0))
      if (index == 0)
         addToHead (item);
      else {
          nPtr->setNext(pPtr->getNext());
          pPtr->setNext(nPtr);
      }
    if (tail == pPtr)
      tail = tail->getNext();
}

template <class T>
bool FCISLL<T>::search(T item) {
    Node<T>* ptr = head;
    while (! (ptr == NULL) && item != ptr->getData())
        ptr = ptr -> getNext();
    if (ptr != NULL)
        return true;
    else
        return false;
}

template <class T>
ostream& operator<<
  (ostream& stream, FCISLL<T> list) {
        if (list.head != 0) {
            Node<T>* ptr = list.head;
            stream << *ptr;
            while (ptr != list.tail) {
                ptr = ptr->getNext();
                stream << *ptr;
            }
            stream << "NULL" << endl;
        }
        return stream;
}

///////////////////////////////
template <class T>
void FCISLL<T>:: save(Task *ptr)    //Function that saves tasks info to file //btktb 3la elfile
 {//btktb 3la el file
     Task *curr=headTask;
    ofstream dataFile;
    dataFile.open("MyTasks.txt", ios::out | ios::app | ios::trunc );
    while(curr->getnext() !=NULL)
    {
    dataFile << ptr->getname() << endl;
    dataFile << ptr->getdescription() << endl;
    dataFile << ptr->getdue_date() << endl;
    dataFile << ptr->getpriority()<< endl;
    dataFile << ptr->getnumOfDays()<< endl;
    dataFile.close();
 }}

///////////////////////////////////////////////
template <class T>
void FCISLL<T>::createLinkedList(Task *data)
{
    int counter =0;
    Task *nodeTask;
    FCISLL<Task>* mySLL1 = new FCISLL<Task>();
    ifstream dataFile;//("MyTasks.txt", ios::in);
    dataFile.open("MyTasks.txt", ios::in);

	        if(!dataFile) //file not open
	            return;


    while(!dataFile.eof());
    {
        nodeTask->setname(data->getname());
        nodeTask->setdescription(data->getdescription());
        nodeTask->setpriority(data->getpriority());
        nodeTask->setdue_date(data->getdue_date());
        nodeTask->setnumOfDays(data->getnumOfDays());

       if (head == NULL)//lw el head bysawy null y3ne asln mfesh nodes fl list
        {
           nodeTask->setnext(headTask);
           headTask=nodeTask; //hna el head b2a byshawer 3la eltask elgdeda ely ana 3mlaha
        }
        else
    {
          Task *curr=headTask;
          priviosTask= NULL;
         while(curr->getnext() !=NULL &&curr->getpriority() < data->getpriority() )
          {
          priviosTask = curr;
          curr = curr->next;
          }
        nodeTask->setnext(curr);
        priviosTask->setnext(nodeTask);
        }
    }
    Task *ptr = headTask->getnext();
    mySLL1.save(ptr);
    dataFile.close();
    }

//////////////////////////////////////////////////////////////
template <class T>
void FCISLL<T>:: save(Task *ptr)    //Function that saves tasks info to file //btktb 3la elfile
 {//btktb 3la el file
     Task *ptr=headTask;
    ofstream dataFile;
    dataFile.open("MyTasks.txt", ios::out | ios::app | ios::trunc );
    while(ptr->getnext() !=NULL)
    {
    dataFile << ptr->getname() << endl;
    dataFile << ptr->getdescription() << endl;
    dataFile << ptr->getdue_date() << endl;
    dataFile << ptr->getpriority()<< endl;
    dataFile << ptr->getnumOfDays()<< endl;

    //dataFile.write(reinterpret_cast<char*>(&ptr), sizeof(ptr));
    dataFile.close();
 }}
//////////////////////////////
template <class T>
Task FCISLL<T>::readTasks() //dy function
{
fstream fout;
fout.open("MyTasks.txt", ios::in);
Task orderedTasks;
fout.read(reinterpret_cast <char*> (&orderedTasks), sizeof(orderedTasks));
while (!fout.eof())
{
   orderedTasks->getname();
   orderedTasks->getdescription();
   orderedTasks->getpriority();
   orderedTasks->getdue_date();
   orderedTasks->getnumOfDays();

   return orderedTasks;
}
//close the file
fout.close();
}
//////////////////////////////////////////////////
int main()
{
    Task *objTask;
    FCISLL<Task>* mySLL = new FCISLL<Task>();
    int piriority,days;
    string name,desc,date;
    cout<<"enter your Task name : "<<endl;
     getline(cin, objTask->setname(name));
    cout<<"enter your Task description : "<<endl;
     getline(cin, objTask->setdescription(desc));
    cout<<"enter your Task Piriority From 1 to 5: "<<endl;
    cin>>objTask->setpriority(piriority);
    cout<<"enter your Task number of Days: "<<endl;
    cin>>objTask->setnumOfDays(days);
    cout << "Enter date: " << endl;
     getline(cin, objTask->setdue_date(date));

    mySLL.createLinkedList(objTask);
    cout<<mySLL.readTasks();//for display

}
