// File: STLList.cpp - Demo of list class
// Author: Mohammad El-Ramly
// Date: 12 March 2013
// Link: http://www.mathcs.duq.edu/drozdek/DSinCpp/

#include <iostream>
#include <list>
#include <conio.h>
#include "FCISLL.cpp"
#include "isPassed_function.h"
using namespace std;

int main(int argc, char *argv[]){
    FCISLL<int>* mySLL = new FCISLL<int>();
    for(int i = 1; i <= 6; i++) {
        mySLL->addToHead(i);
        cout << *mySLL;
        getch();
    }
    for(int i = 111; i <= 115; i++) {
        mySLL->addToTail(i);
        cout << *mySLL;
        getch();
    }
    // removeHead()
	Node<int>* Head = mySLL->removeHead();
	if(Head!=NULL)
	{
		cout << "Head of Value -> " << Head->getData() << " is removed" << endl;
		delete Head;
		cout << *mySLL;
		getch();
	}

	//  removeTail()
	Node<int>* Tail = mySLL->removeTail();
	if(Tail!=NULL)
	{
	    cout << "Tail of Value -> " << Tail->getData() << " is removed "<< endl;
 		delete Tail;
		cout<<*mySLL;
		getch();
	}

	//  removeFromIndex()
	int index;
	cout << "Enter index to remove: ";
    cin >> index;
	Node<int>* ptr_index = mySLL->removeFromIndex(index);
	if(ptr_index!=NULL)
	{
	    cout << "index "<< index << "of Value -> " << ptr_index->getData() << " is removed " << endl;
        delete ptr_index;
		cout<<*mySLL;
	}
	else
		cout << "Invalid index" << endl;
	getch();

	// removeItem()
	int item;
	cout << "Enter value to remove: ";
    cin >> item;
    Node<int>* ptr_item = mySLL->removeItem(item);
	if(ptr_item!=NULL)
	{
        cout << "item of Value -> " <<item<< " is removed "<< endl;
		delete ptr_item;
		cout<<*mySLL;
	}
	else
		cout << "Invalid value" << endl;
	getch();


//  removeWithPredicate()
/*
	mySLL->removeWithPredicate(isPassed);
	cout << "Remove with predicate (task)" << endl;
	cout<<*mySLL;
	getch();
*/
    cout << "Press the enter key to continue ...";
    cin.get();
    return EXIT_SUCCESS;
}
