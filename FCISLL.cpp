// Program: FCISLL is a generic single linked list
//          Developed for CS214 Data Structures
//          at FCI, Cairo University
// Author:  Mohammad El-Ramly, 8 Mar. 2013
// Link: http://www.mathcs.duq.edu/drozdek/DSinCpp/

#include <iostream>
#include <conio.h>
#include <stdlib.h>
#include "FCISLL.h"

using namespace std;

template <class T>
ostream& operator<< (ostream& out, Node<T> node) {
    out << node.getData() << "->";
    return out;
}

////////////////////////////////
template <class T>
void FCISLL<T>::addToTail(T item) {
    Node<T>* ptr = new Node<T>(item, NULL);
    if (head == NULL)
        head = tail = ptr;
    else {
        tail->setNext(ptr);
        tail = ptr;
    }
}

template <class T>
void FCISLL<T>::addToHead(T item) {
    Node<T>* ptr = new Node<T>(item, head);
    head = ptr;
    if (tail == NULL)
        tail = ptr;
}

template <class T>
FCISLL<T>::~FCISLL () {}

template <class T>
void FCISLL<T>::addToIndex(T item, int index) {
    Node<T>* nPtr = new Node<T>(item, NULL);
    Node<T>* pPtr = head;
    for (int i = 0; pPtr != 0 && i < index - 1; i++)
       pPtr = pPtr->getNext();
    if (pPtr != 0 && !(index < 0))
      if (index == 0)
         addToHead (item);
      else {
          nPtr->setNext(pPtr->getNext());
          pPtr->setNext(nPtr);
      }
    if (tail == pPtr)
      tail = tail->getNext();
}

template <class T>
bool FCISLL<T>::search(T item) {
    Node<T>* ptr = head;
    while (! (ptr == NULL) && item != ptr->getData())
        ptr = ptr -> getNext();
    if (ptr != NULL)
        return true;
    else
        return false;
}
// Remove from head

template <class T>
Node<T>* FCISLL<T>::removeHead ()
{
	Node<T>* ptr = head;

	if(head!=NULL)
		head = head->getNext();

	return ptr;
}

// Remove from tail

template <class T>
Node<T>* FCISLL<T>::removeTail ()
{
	Node<T> *ptr, *prev;
    ptr = prev = head;

	while(ptr!=NULL && ptr->getNext()!=NULL)
	{
		prev = ptr;
		ptr = ptr->getNext();
	}

	if(ptr==prev)
		tail = head = NULL;
	else
	{
		tail = prev;
		tail->setNext(NULL);
	}

	return ptr;
}

// remove from index


template <class T>
Node<T>* FCISLL<T>::removeFromIndex (int index) // Make sure the index is valid
{
	if(index<0 || head==NULL)
		return NULL;

	if(index==0)
		return removeHead();


	Node<T> *ptr, *prev;
	prev = head;
	ptr = head->getNext();
	for(int i=1; i<index && ptr!=NULL; i++)
	{
		prev = ptr;
		ptr = ptr->getNext();
	}



		prev->setNext(ptr->getNext());
		if(ptr==tail)
			tail = prev;
		return ptr;

}


// Remove first instance of item
template <class T>
Node<T>* FCISLL<T>::removeItem (T item)
{
	if(head==NULL)
		return NULL;
	else if(head->getData()==item)
		return removeHead();
	else
	{
		Node<T> *ptr, *prev;
		ptr = head;
		while (! (ptr == NULL) && item != ptr->getData())
		{
			prev = ptr;
			ptr = ptr -> getNext();
		}
		if (ptr != NULL)
		{
			prev->setNext(ptr->getNext());
			if(ptr==tail)
				tail = prev;
			return ptr;
		}
		else
			return NULL;
	}
}

// remove withPredicate
template <class T>
void FCISLL<T>::removeWithPredicate (bool (*predicate)(T& item))
{
	Node<T> *ptr = head;
	int index=0;
	T item;
	while(ptr!=NULL)
	{
		item = ptr->getData();
		ptr = ptr->getNext();
		if(predicate(item)==true)
		{
			removeFromIndex(index);
		}
		else
			index++;
	}
}


template <class T>
ostream& operator<<
  (ostream& stream, FCISLL<T> list) {
        if (list.head != 0) {
            Node<T>* ptr = list.head;
            stream << *ptr;
            while (ptr != list.tail) {
                ptr = ptr->getNext();
                stream << *ptr;
            }
            stream << "NULL" << endl;
        }
        return stream;
}

///////////////////////////////

