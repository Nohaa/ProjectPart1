// Program: FCISLL is a generic single linked list
//          Developed for CS214 Data Structures
//          at FCI, Cairo University
// Author:  Mohammad El-Ramly, 8 Mar. 2013

#ifndef FCISLL_H_
#define FCISLL_H_

#include <iostream>
#include <conio.h>
#include <stdlib.h>

using namespace std;

template <class T>
class Node {
  protected:
	T     data;        // to hold generic data of type T
	Node<T>* next;     // to hold pointer to next item
	Node<T>* prev;     // to hold pointer for previous item
	int   count;
  public:
		Node () {next = prev = NULL; count = 0;}
		Node (T dataItem, Node<T>* nextPtr = NULL, Node<T>* prevPtr = NULL) {
            data = dataItem; next = nextPtr; prev = prevPtr;
        }
		Node<T>* getNext ()        {return next;}
		void setNext(Node<T>* ptr) {next = ptr;}
		Node<T>* getPrev ()        {return prev;}
		void setPrev(Node<T>* ptr) {prev = ptr;}
		T  getData ()              {return data;}
		void setData (T dataItem)  {data = dataItem;}
		template <class TT>
		friend ostream& operator<< (ostream& out, Node<TT> n);
};

/////////////////////////////
template <class T>
class FCISLL {
    private:
        Node<T> *head, *tail;
    public:
        FCISLL() {head = NULL; tail = NULL;}
        ~FCISLL ();
        void addToHead(T item);
        void addToTail(T item);
        void addToIndex(T item, int index);
        bool search (T item);
        Node<T>* removeHead ();
		Node<T>* removeTail ();
		Node<T>* removeFromIndex (int index); // Make sure the index is valid
		Node<T>* removeItem (T item); // Remove first instance of item
		void removeWithPredicate (bool (*predicate)(T& item)); // Remove first instance of item

        template <class TT>
        friend ostream& operator<<(ostream&, FCISLL<TT>);
};

//////////////////////////////

#endif
